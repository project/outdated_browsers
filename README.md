CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Maintainers


INTRODUCTION
------------

This module allows to redirect all requests to a custom page if the client browser is not supported by the Drupal application. 
It also detects Bots used for SEO.

REQUIREMENTS
------------

Universal Device Detection module https://www.drupal.org/project/universal_device_detection.


INSTALLATION
------------

Install the Outdated Browsers module as you would normally install
any Drupal contrib module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

To configure this module, just go to the configuration page (/admin/config/system/outdated-browsers) and specify the minimum versions supported by the application,
and fill the title and content of the page that will be displayed to the client in case the browser is outdated.


USAGE
--------------

None

MAINTAINERS
-----------

This module was created and sponsored by nassaz,

* Nassaz - https://www.nassaz.com/
