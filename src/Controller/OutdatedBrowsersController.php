<?php

namespace Drupal\outdated_browsers\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides the information page when the client's browser is out of date.
 */
class OutdatedBrowsersController extends ControllerBase {

  /**
   * Returns the information page.
   *
   * @return array
   *   Renderer array
   */
  public function informationPage() {
    return [
      '#theme' => 'html',
    ];
  }

}
