<?php

namespace Drupal\outdated_browsers\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure outdated browsers.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'outdated_browsers_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'outdated_browsers.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    // Get current configuration.
    $outdated_browsers_config = $this->config('outdated_browsers.settings');

    $form['information'] = [
      '#type'       => 'container',
      '#attributes' => [],
      '#children'   => $this->t('Set the minimum supported versions here.'),
    ];

    // Set which versions will be redirected.
    $form['notify'] = [
      '#type'                    => 'details',
      '#open'                    => TRUE,
      '#title'                   => $this->t('Minimum browser versions'),
      '#description'             => $this->t("Below these versions, the user will be prompted to update their browser."),
      'ie_version'               => [
        '#type'          => 'number',
        '#title'         => $this->t('Internet Explorer'),
        '#default_value' => $outdated_browsers_config->get('ie_version'),
      ],
      'edge_version'             => [
        '#type'          => 'number',
        '#title'         => $this->t('Edge'),
        '#default_value' => $outdated_browsers_config->get('edge_version'),
      ],
      'firefox_version'          => [
        '#type'          => 'number',
        '#title'         => $this->t('Firefox'),
        '#default_value' => $outdated_browsers_config->get('firefox_version'),
      ],
      'mobile_firefox_version'  => [
        '#type'          => 'number',
        '#title'         => $this->t('Firefox mobile'),
        '#default_value' => $outdated_browsers_config->get('mobile_firefox_version'),
      ],
      'opera_version'            => [
        '#type'          => 'number',
        '#title'         => $this->t('Opera'),
        '#default_value' => $outdated_browsers_config->get('opera_version'),
      ],
      'safari_version'           => [
        '#type'          => 'number',
        '#title'         => $this->t('Safari'),
        '#default_value' => $outdated_browsers_config->get('safari_version'),
      ],
      'chrome_version'           => [
        '#type'          => 'number',
        '#title'         => $this->t('Chrome'),
        '#default_value' => $outdated_browsers_config->get('chrome_version'),
      ],
      'ios_safari_version'       => [
        '#type'          => 'number',
        '#title'         => $this->t('iOS Safari'),
        '#default_value' => $outdated_browsers_config->get('ios_safari_version'),
      ],
      'android_browser_version'  => [
        '#type'          => 'number',
        '#title'         => $this->t('Android Browser'),
        '#default_value' => $outdated_browsers_config->get('android_browser_version'),
      ],
      'samsung_internet_version' => [
        '#type'          => 'number',
        '#title'         => $this->t('Samsung Internet'),
        '#default_value' => $outdated_browsers_config->get('samsung_internet_version'),
      ],

    ];

    $form['message'] = [
      '#type'   => 'details',
      '#open'   => FALSE,
      '#title'  => $this->t('Message'),
      'title'   => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Title'),
        '#default_value' => $outdated_browsers_config->get('title'),
      ],
      'message' => [
        '#type'          => 'text_format',
        '#title'         => $this->t('Message to display'),
        '#description'   => $this->t("Write here the message intended for the user having an obsolete browser."),
        '#default_value' => $outdated_browsers_config->get('message.value'),
        '#format'        => $outdated_browsers_config->get('message.format'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @ todo.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated browsers settings.
    $this->config('outdated_browsers.settings')
      ->set('ie_version', $values['ie_version'])
      ->set('edge_version', $values['edge_version'])
      ->set('firefox_version', $values['firefox_version'])
      ->set('opera_version', $values['opera_version'])
      ->set('safari_version', $values['safari_version'])
      ->set('chrome_version', $values['chrome_version'])
      ->set('ios_safari_version', $values['ios_safari_version'])
      ->set('mobile_firefox_version', $values['mobile_firefox_version'])
      ->set('android_browser_version', $values['android_browser_version'])
      ->set('samsung_internet_version', $values['samsung_internet_version'])
      ->set('title', $values['title'])
      ->set('message.value', $values['message']['value'])
      ->set('message.format', $values['message']['format'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
