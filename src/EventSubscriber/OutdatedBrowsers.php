<?php

namespace Drupal\outdated_browsers\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\Config;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\universal_device_detection\Detector\DeviceDetectorInterface;

/**
 * A subscriber redirecting requests if the browser is obsolete.
 */
class OutdatedBrowsers implements EventSubscriberInterface {

  /**
   * The outdated browsers' configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The universal device detection.
   *
   * @var \Drupal\universal_device_detection\Detector\DeviceDetectorInterface
   */
  protected DeviceDetectorInterface $deviceDetector;

  /**
   * Constructs a new outdated browsers' subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\universal_device_detection\Detector\DeviceDetectorInterface $device_detector
   *   The universal device detection.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteMatchInterface $route_match, DeviceDetectorInterface $device_detector) {
    $this->config = $config_factory->get('outdated_browsers.settings');
    $this->routeMatch = $route_match;
    $this->deviceDetector = $device_detector;
  }

  /**
   * Redirects requests if the browser is obsolete.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   An event object.
   */
  public function redirect(RequestEvent $event) {

    // Get the route name.
    $route_name = $this->routeMatch->getRouteName();

    // Get the client browser information.
    $device = $this->deviceDetector->detect();

    // Detect Bots.
    if (array_key_exists('type', $device) && $device['type'] === 'bot') {
      return;
    }

    $browser = $version = NULL;

    if (array_key_exists('client', $device['info'])) {
      $browser = $device['info']['client']['name'];
      $version = (int) $device['info']['client']['version'];
    }

    $outdated = match ($browser) {
      'Internet Explorer' => $version < $this->config->get('ie_version'),
      'Microsoft Edge' => $version < $this->config->get('edge_version'),
      'Chrome' => $version < $this->config->get('chrome_version'),
      'Firefox' => $version < $this->config->get('firefox_version'),
      'Firefox Mobile' => $version < $this->config->get('mobile_firefox_version'),
      'Opera' => $version < $this->config->get('opera_version'),
      'Safari' => $version < $this->config->get('safari_version'),
      'Mobile Safari' => $version < $this->config->get('ios_safari_version'),
      'Android Browser' => $version < $this->config->get('android_browser_version'),
      'Samsung Browser' => $version < $this->config->get('samsung_internet_version'),
      NULL => TRUE,
      default => FALSE,
    };

    // Redirect to the information page.
    if ($outdated && $route_name !== "outdated_browsers.information_page") {
      $path = Url::fromRoute('outdated_browsers.information_page')
        ->toString();
      $response = new RedirectResponse($path);
      $response->send();
    }

    // Redirect to the front page if the browser is supported.
    if (!$outdated && $route_name === "outdated_browsers.information_page") {
      $path = Url::fromRoute('<front>')->toString();
      $response = new RedirectResponse($path);
      $response->send();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => [['redirect']]];
  }

}
